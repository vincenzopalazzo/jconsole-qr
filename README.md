# JQRInterface
![Maven Central with version prefix filter](https://img.shields.io/maven-central/v/io.github.vincenzopalazzo/JQRInterface/0.0.1-rc1?style=for-the-badge)

It is a Java Interface that help to generate QR code from a String with different format!

## Format supported at the moment

- [X] Image (awt).
- [X] ASCII String (Console).
- [X] Array of bytes (to build an image).
- [ ] PDF.
- [ ] Image (JavaFX).

Look how use the library inside demos [here](https://gitlab.com/vincenzopalazzo/jconsole-qr/-/tree/master/src/test/java/io/vincenzopalazzo/qr)

## Maven repository

### Maven
```xml
<dependency>
  <groupId>io.github.vincenzopalazzo</groupId>
  <artifactId>JQRInterface</artifactId>
  <version>0.0.1-rc1</version>
</dependency>
```

## Gradle Groovy DSL
```groovy
implementation 'io.github.vincenzopalazzo:JQRInterface:0.0.1-rc1'
```

## Gradle Kotlin DSL
```kotlin
implementation("io.github.vincenzopalazzo:JQRInterface:0.0.1-rc1")
```

[other information on Maven repository](https://search.maven.org/artifact/io.github.vincenzopalazzo/JQRInterface)

## Dev

Feel Free to open an issue or an PR to propose or add new conversion type. In addition, 
I'm searching a maintainer for this repository, feel free to send me an email to [vincenzopalazzodev@gmai.com](mailito://vincenzopalazzodev@gmai.com) 
if you want try to work on this library!

## Code Style
> We live in a world where robots can drive a car, so we shouldn't just write code, we should write elegant code.

This repository use [google-java-format](https://github.com/sherter/google-java-format-gradle-plugin) to maintains the code of the repository elegant, so
before submit the code check the Java format with the following command on the root of the directory

```bash
./gradlew verifyGoogleJavaFormat
```

It any error are reported please run the following command to try to fix it

```bash
./gradlew googleJavaFormat
```

p.s: The gradle plugin work with all the JDK version >= 9 (or better with java byte code version compatible with the version  55.0)

For more details about the JDK support see the [this issue](https://github.com/sherter/google-java-format-gradle-plugin/issues/58)
and to know more about the Google Java code Style see the [this reference](https://google.github.io/styleguide/javaguide.html)

## Support
If you like the library and want support it, please considerer to donate with the following system

- [liberapay.com/vincenzopalazzo](https://liberapay.com/vincenzopalazzo)
- [3BQ8qbn8hLdmBKEjt1Hj1Z6SiDsnjJurfU](bitcoin:3BQ8qbn8hLdmBKEjt1Hj1Z6SiDsnjJurfU)
- [Github support](https://github.com/sponsors/vincenzopalazzo)

## License

<div align="center">
  <img src="https://opensource.org/files/osi_keyhole_300X300_90ppi_0.png" width="150" height="150"/>
</div>

 Simple QR interface written in java to build a QR with different format
 
 Copyright (C) 2020 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
