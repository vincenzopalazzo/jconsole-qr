/**
 * Simple QR interface written in java to build a QR with different format Copyright (C) 2020
 * Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
package io.vincenzopalazzo.qr;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import io.vincenzopalazzo.qr.exception.JQRException;
import io.vincenzopalazzo.qr.util.Converter;
import io.vincenzopalazzo.qr.util.exception.UtilException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Hashtable;

/** @author https://github.com/vincenzopalazzo */
public class QRCode {

  public static Image getQrToImage(String string, int width, int height) throws JQRException {
    try {
      BitMatrix qrFromString = getQrObject(string, width, height);
      BufferedImage image =
          new BufferedImage(
              qrFromString.getWidth(), qrFromString.getHeight(), BufferedImage.TYPE_INT_RGB);
      image.createGraphics();
      Graphics2D graphics = (Graphics2D) image.getGraphics();
      graphics.setColor(Color.WHITE);
      graphics.fillRect(0, 0, qrFromString.getWidth(), qrFromString.getHeight());
      // Paint and save the image using the ByteMatrix
      graphics.setColor(Color.BLACK);
      for (int i = 0; i < qrFromString.getWidth(); i++) {
        for (int j = 0; j < qrFromString.getWidth(); j++) {
          if (qrFromString.get(i, j)) {
            graphics.fillRect(i, j, 1, 1);
          }
        }
      }
      return image;
    } catch (WriterException e) {
      throw new JQRException(e.getCause());
    }
  }

  public static String getQrToString(String string, int width, int height) throws JQRException {
    try {
      BitMatrix qrFromString = getQrObject(string, width, height);
      return Converter.toAsci(qrFromString);
    } catch (WriterException e) {
      throw new JQRException(e.getCause());
    }
  }

  public static String getQrToAsci(String string, int width, int height, String white, String black)
      throws JQRException {
    try {
      BitMatrix qrFromString = getQrObject(string, width, height);
      return Converter.toAsci(qrFromString, white, black);
    } catch (WriterException e) {
      throw new JQRException(e.getCause());
    }
  }

  public static byte[] getQrToBytes(String string, int width, int height) throws JQRException {
    Image qrImahe = getQrToImage(string, width, height);
    try {
      byte[] bytesImage = Converter.getBytesFromImages(qrImahe);
      return bytesImage;
    } catch (UtilException e) {
      throw new JQRException(e.getCause());
    }
  }

  public static BitMatrix getQrObject(String string, int width, int height) throws WriterException {
    Hashtable<EncodeHintType, Object> qrParam = new Hashtable<>();
    qrParam.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
    qrParam.put(EncodeHintType.CHARACTER_SET, "utf-8");
    return new MultiFormatWriter().encode(string, BarcodeFormat.QR_CODE, width, height, qrParam);
  }
}
