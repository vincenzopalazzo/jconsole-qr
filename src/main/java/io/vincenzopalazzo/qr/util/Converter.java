/**
 * Simple QR interface written in java to build a QR with different format Copyright (C) 2020
 * Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
package io.vincenzopalazzo.qr.util;

import com.google.zxing.common.BitMatrix;
import io.vincenzopalazzo.qr.util.exception.UtilException;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

/** @author https://github.com/vincenzopalazzo */
public class Converter {

  public static byte[] getBytesFromImages(Image image) throws UtilException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
      ImageIO.write((RenderedImage) image, "jpeg", baos);
      baos.flush();
      return baos.toByteArray();
    } catch (IOException e) {
      throw new UtilException(e.getCause());
    }
  }

  public static String toAsci(BitMatrix bitMatrix) {
    return Converter.toAsci(bitMatrix, "\033[47m  \033[0m", "\033[40m  \033[0m");
  }

  public static String toAsci(BitMatrix bitMatrix, String white, String black) {
    StringBuilder sb = new StringBuilder();
    for (int rows = 0; rows < bitMatrix.getHeight(); rows++) {
      for (int cols = 0; cols < bitMatrix.getWidth(); cols++) {
        boolean x = bitMatrix.get(rows, cols);
        if (!x) {
          // white
          sb.append(white);
        } else {
          // blacj
          sb.append(black);
        }
      }
      sb.append("\n");
    }
    return sb.toString();
  }
}
