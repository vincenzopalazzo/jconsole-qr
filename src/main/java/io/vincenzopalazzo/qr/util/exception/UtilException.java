/**
 * Simple QR interface written in java to build a QR with different format Copyright (C) 2020
 * Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
package io.vincenzopalazzo.qr.util.exception;

/** @author https://github.com/vincenzopalazzo */
public class UtilException extends Exception {
  public UtilException() {}

  public UtilException(String message) {
    super(message);
  }

  public UtilException(String message, Throwable cause) {
    super(message, cause);
  }

  public UtilException(Throwable cause) {
    super(cause);
  }

  public UtilException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
