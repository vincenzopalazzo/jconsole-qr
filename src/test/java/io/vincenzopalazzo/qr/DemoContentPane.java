/**
 * Simple QR interface written in java to build a QR with different format Copyright (C) 2020
 * Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
package io.vincenzopalazzo.qr;

import io.vincenzopalazzo.qr.exception.JQRException;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.*;
import org.material.component.linklabelui.model.LinkLabel;
import org.material.component.swingsnackbar.action.AbstractSnackBarAction;

/** @author https://github.com/vincenzopalazzo */
public class DemoContentPane extends JPanel {

  private Image qr;
  private LinkLabel label;
  private String contentQr;

  public DemoContentPane() {
    contentQr = "https://gitlab.com/vincenzopalazzo/jconsole-qr";
    initView();
  }

  public void initView() {
    label = new LinkLabel(contentQr);
    try {
      qr = QRCode.getQrToImage(contentQr, 140, 140);
    } catch (JQRException e) {
      e.printStackTrace();
    }
    initLayout();
    initAction();
    this.setVisible(true);
  }

  public void initLayout() {
    this.setLayout(new BorderLayout());
    this.add(label, BorderLayout.NORTH);
    this.add(new JLabel(new ImageIcon(qr)), BorderLayout.CENTER);
  }

  public void initAction() {
    label.addMouseListener(
        new AbstractSnackBarAction() {
          @Override
          public void mousePressed(MouseEvent e) {
            // Create Desktop object
            Desktop d = Desktop.getDesktop();

            try {
              d.browse(new URI(contentQr));
            } catch (IOException ioException) {
              ioException.printStackTrace();
            } catch (URISyntaxException uriSyntaxException) {
              uriSyntaxException.printStackTrace();
            }
          }
        });
  }

  public String getContentQr() {
    return contentQr;
  }
}
