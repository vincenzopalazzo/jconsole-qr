/**
 * Simple QR interface written in java to build a QR with different format Copyright (C) 2020
 * Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 */
package io.vincenzopalazzo.qr;

import io.materialtheme.darkstackoverflow.DarkStackOverflowTheme;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.*;
import javax.swing.plaf.IconUIResource;
import mdlaf.MaterialLookAndFeel;
import mdlaf.utils.MaterialColors;
import mdlaf.utils.MaterialImageFactory;
import mdlaf.utils.icons.MaterialIconFont;
import org.material.component.swingsnackbar.SnackBar;
import org.material.component.swingsnackbar.action.AbstractSnackBarAction;

/** @author https://github.com/vincenzopalazzo */
public class DemoGUI extends JFrame {

  static {
    try {
      UIManager.setLookAndFeel(new MaterialLookAndFeel(new DarkStackOverflowTheme()));
    } catch (UnsupportedLookAndFeelException e) {
      e.printStackTrace();
    }
  }

  private static final DemoGUI SINGLETON = new DemoGUI();

  public static DemoGUI getInstance() {
    return SINGLETON;
  }

  private DemoContentPane contentPane;

  private void initApp() {
    contentPane = new DemoContentPane();
    this.setContentPane(contentPane);
    IconUIResource icon =
        MaterialImageFactory.getInstance()
            .getImage(MaterialIconFont.SEND, 25, MaterialColors.COSMO_BLUE);
    this.setSize(1000, 750);
    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setVisible(true);

    SnackBar.make(this, contentPane.getContentQr(), icon)
        .setDuration(SnackBar.LENGTH_LONG)
        .setMarginBottom(10)
        .setAction(
            new AbstractSnackBarAction() {
              @Override
              public void mousePressed(MouseEvent e) {
                // Create Desktop object
                Desktop d = Desktop.getDesktop();

                try {
                  d.browse(new URI(contentPane.getContentQr()));
                } catch (IOException ioException) {
                  ioException.printStackTrace();
                } catch (URISyntaxException uriSyntaxException) {
                  uriSyntaxException.printStackTrace();
                }
              }
            })
        .run();
  }

  public static void main(String[] args) {
    SwingUtilities.invokeLater(
        new Runnable() {
          @Override
          public void run() {
            SINGLETON.initApp();
          }
        });
  }
}
