plugins {
    id("com.github.sherter.google-java-format") version "0.9"
    `java-library`
    `maven-publish`
    signing
}

repositories {
    jcenter()
    mavenCentral()
}

version = project.property("PROJECT_VERSION")!!
group = project.property("GROUP_ID")!!

/*
java {
    modularity.inferModulePath.set(true)
}
*/

dependencies {
    implementation("com.google.zxing:core:3.4.0")
    implementation("com.google.zxing:javase:3.4.0")

    testImplementation("io.github.vincenzopalazzo:material-ui-swing:1.1.1")
    testImplementation("io.github.material-ui-swing:DarkStackOverflowTheme:0.0.1-rc2")
    testImplementation("io.github.material-ui-swing:SwingSnackBar:0.0.1-rc6")
    testImplementation("junit:junit:4.12")
    testImplementation("io.github.material-ui-swing:LinkLabelUI:0.0.1-rc1")
}

//This need to use java9 module with gradle 6.4.1
plugins.withType<JavaPlugin>().configureEach {
    configure<JavaPluginExtension> {
        modularity.inferModulePath.set(true)
    }
}

tasks.jar {
    manifest {
        attributes("Automatic-Module-Name" to project.property("MODULE_NAME")!!)
    }
}

tasks{
    create<Jar>("sourcesJar") {
        archiveClassifier.set("sources")
        from(sourceSets["main"].allSource)
    }

    create<Jar>("javadocJar") {
        archiveClassifier.set("javadoc")
        from(sourceSets["main"].allSource)
    }

    withType<JavaCompile>().configureEach {
        options.encoding = "ISO-8859-1"
    }

    withType<Jar>().configureEach {
        // add META-INF/LICENSE to all created JARs
        from("${rootDir}/LICENSE") {
            into("META-INF")
        }
    }
}

publishing {
    publications {
        create<MavenPublication>(project.name) {
            from(components["java"])

            artifact(tasks["sourcesJar"])
            artifact(tasks["javadocJar"])

            repositories {
                maven {
                    credentials {
                        username = project.property("sonatypeUsername").toString()
                        password = project.property("sonatypePassword").toString()
                    }
                    val releasesRepoUrl = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2/")
                    val snapshotsRepoUrl = uri("https://oss.sonatype.org/content/repositories/snapshots/")
                    url = if (version.toString().endsWith("SNAPSHOT")) snapshotsRepoUrl else releasesRepoUrl
                }
            }

            pom {
                name.set(project.name)
                description.set("It is a Java Interface that help to generate QR code from a String with different format!")
                url.set("https://gitlab.com/vincenzopalazzo/jconsole-qr")

                licenses {
                    license {
                        name.set("GNU General Public License v2.0")
                        url.set("https://gitlab.com/vincenzopalazzo/jconsole-qr/-/blob/master/LICENSE.md")
                    }
                }

                developers {
                    developer {
                        id.set("vincenzopalazzo")
                        name.set("Vincenzo Palazzo")
                        email.set("vincenzopalazzodev@gmail.com")
                        url.set("https://github.com/vincenzopalazzo")
                        roles.addAll("developer")
                        timezone.set("Europe/Rome")
                    }
                }

                scm {
                    connection.set("git@gitlab.com:vincenzopalazzo/jconsole-qr.git")
                    developerConnection.set("git@gitlab.com:vincenzopalazzo/jconsole-qr.git")
                    url.set("https://gitlab.com/vincenzopalazzo/jconsole-qr.git")
                }
            }
        }
    }
}

signing {
    isRequired = true
    sign(tasks["sourcesJar"], tasks["javadocJar"])
    sign(publishing.publications[project.name])
}

tasks {
    register("fatJar", Jar::class.java) {
        archiveClassifier.set("all")
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
        manifest {
            attributes("Automatic-Module-Name" to project.property("MODULE_NAME")!!)
        }
        from(configurations.runtimeClasspath.get()
                .onEach { println("add from dependencies: ${it.name}") }
                .map { if (it.isDirectory) it else zipTree(it) })
        val sourcesMain = sourceSets.main.get()
        sourcesMain.allSource.forEach { println("add from sources: ${it.name}") }
        from(sourcesMain.output)
    }
}

